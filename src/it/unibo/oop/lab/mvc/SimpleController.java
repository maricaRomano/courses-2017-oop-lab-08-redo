package it.unibo.oop.lab.mvc;

import java.util.LinkedList;
import java.util.List;

public class SimpleController implements Controller {
	
	private List<String> hstStrings;
	private String nextString;
	
	public SimpleController() {
		hstStrings = new LinkedList<>();
		hstStrings.add("Debug string");
	}

	@Override
	public String getNextString() {
		// TODO Auto-generated method stub
		return this.nextString;
	}

	@Override
	public List<String> history() {
		// TODO Auto-generated method stub
		return hstStrings;
	}

	@Override
	public void printCurrentString() throws IllegalArgumentException {
		// TODO Auto-generated method stub
		if(this.nextString == null) {
			throw new IllegalArgumentException("Cannot print null value");
		} else {
			hstStrings.add(nextString);
			System.out.println(this.nextString);
		}
	}

	@Override
	public void setNextString(String string) {
		// TODO Auto-generated method stub
		this.nextString = string;
	}

}
