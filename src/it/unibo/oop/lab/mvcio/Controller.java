package it.unibo.oop.lab.mvcio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * 
 */
public class Controller {

    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class in such a way that:
     * 
     * 1) It has a method for setting a File as current file
     * 
     * 2) It has a method for getting the current File
     * 
     * 3) It has a method for getting the path (in form of String) of the current
     * File
     * 
     * 4) It has a method that gets a Serializable as input and saves such Object in
     * the current file. Remember how to use the ObjectOutputStream. This method may
     * throw IOException.
     * 
     * 5) By default, the current file is "output.dat" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that run correctly on every platform.
     */
	private final static String SEPARATOR = System.getProperty("file.separator");
	private final static String HOME_FOLDER = System.getProperty("user.home");
	private final static String NAME_FILE = "output.txt";
	
	private File file;
	
	public Controller() {
		this.file = new File(HOME_FOLDER + SEPARATOR + NAME_FILE);
	}
	
	public File getCurrentFile() {
		return file;
	}
	
	public File setCurrentFile(final File newFile) {
		return file = newFile;
	}
	
	public String getFilePath() {
		return file.getPath();
	}
	
	public void save(final Serializable obj) throws IOException {
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
			oos.writeObject(obj);
		}
	}
	
	public void setDestination(final File file) {
		final File parent = file.getParentFile();
		if(parent.exists()) {
			this.file = file;
		} else {
			throw new IllegalArgumentException("Cannot save 'cause there's no folder");
		}
	}
	
	public void setDestination(final String file) {
		setDestination(new File(file));
	}
}
